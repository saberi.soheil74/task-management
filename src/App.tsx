import { Box, Checkbox, Container, FormControlLabel } from '@mui/material';
import Task from 'interfaces/task';
import { useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import AddTask from './components/add-task';
import TaskList from './components/task-list';

function App() {
  const [tasks, setTasks] = useState<Task[]>([]);

  const [showCompleted, setShowCompleted] = useState<boolean>(true);

  const handleAddTask = (title: string, description: string) => {
    setTasks((prevTasks) => [...prevTasks, { title, description, completed: false }]);
  };

  const handleCheck = (index: number) => {
    setTasks((prevTasks) => {
      const updatedTasks = [...prevTasks];
      updatedTasks[index].completed = !updatedTasks[index].completed;
      return updatedTasks;
    });
  };

  const handleMoveTask = (dragIndex: number, hoverIndex: number) => {
    const updatedTasks = [...tasks];
    const movedTask = updatedTasks.splice(dragIndex, 1)[0];
    updatedTasks.splice(hoverIndex, 0, movedTask);
    setTasks(updatedTasks);
  };

  const handleFilterChange = () => {
    setShowCompleted(!showCompleted);
  };

  const filteredTasks = showCompleted ? tasks : tasks.filter((task) => !task.completed);

  return (
    <DndProvider backend={HTML5Backend}>
      <Container maxWidth="sm">
        <h1>Task Management Application</h1>
        <AddTask onAddTask={handleAddTask} />
        <Box my={2}>
          <FormControlLabel
            control={<Checkbox checked={showCompleted} onChange={handleFilterChange} />}
            label="Show Completed Tasks"
          />
        </Box>
        <Box my={4}>
          <TaskList tasks={filteredTasks} onCheck={handleCheck} onMoveTask={handleMoveTask} />
        </Box>
      </Container>
    </DndProvider>
  );
}

export default App;
