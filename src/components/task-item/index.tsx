import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import { Card, Checkbox, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import Task from 'interfaces/task';
import { FC } from 'react';
import { useDrag, useDrop } from 'react-dnd';

interface TaskItemProps {
  task: Task;
  index: number;
  onCheck: (index: number) => void;
  moveTask: (dragIndex: number, hoverIndex: number) => void;
}

const TaskItem: FC<TaskItemProps> = ({ task, index, onCheck, moveTask }) => {
  const [, ref] = useDrag({
    type: 'task',
    item: { index },
  });

  const [, drop] = useDrop({
    accept: 'task',
    hover: (item: { index: number }) => {
      if (item.index !== index) {
        moveTask(item.index, index);
        item.index = index;
      }
    },
  });

  return (
    <Card sx={{ my: 2, p: 2, bgcolor: '#fefefe', cursor: 'pointer' }}>
      <ListItem ref={(node) => ref(drop(node))} disablePadding>
        <Checkbox checked={task.completed} onChange={() => onCheck(index)} />
        <ListItemText primary={task.title} secondary={task.description} />
        <ListItemIcon>
          <DragIndicatorIcon />
        </ListItemIcon>
      </ListItem>
    </Card>
  );
};

export default TaskItem;
