import { List } from '@mui/material';
import { FC } from 'react';
import TaskItem from '../task-item';

interface Task {
  title: string;
  description: string;
  completed: boolean;
}

interface TaskListProps {
  tasks: Task[];
  onCheck: (index: number) => void;
  onMoveTask: (dragIndex: number, hoverIndex: number) => void;
}

const TaskList: FC<TaskListProps> = ({ tasks, onCheck, onMoveTask }) => {
  return (
    <List>
      {tasks.map((task, index) => (
        <TaskItem key={index} index={index} task={task} onCheck={onCheck} moveTask={onMoveTask} />
      ))}
    </List>
  );
};

export default TaskList;
