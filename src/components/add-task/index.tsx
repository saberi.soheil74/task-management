import { Button, TextField } from '@mui/material';
import { ChangeEvent, FC, useState } from 'react';

interface InputsType {
  title: string;
  description: string;
}

interface AddTaskFormProps {
  onAddTask: (title: string, description: string) => void;
}

const AddTask: FC<AddTaskFormProps> = ({ onAddTask }) => {
  const [inputs, setInputs] = useState<InputsType>({
    title: '',
    description: '',
  });

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (inputs.title.trim() !== '') {
      onAddTask(inputs.title, inputs.description);
      setInputs({ title: '', description: '' });
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <TextField
        label="Title"
        value={inputs.title}
        onChange={(e: ChangeEvent<HTMLInputElement>) => setInputs({ ...inputs, title: e.target.value })}
        variant="outlined"
        margin="normal"
        required
        fullWidth
      />
      <TextField
        label="Description"
        value={inputs.description}
        onChange={(e: ChangeEvent<HTMLInputElement>) => setInputs({ ...inputs, description: e.target.value })}
        variant="outlined"
        margin="normal"
        fullWidth
        multiline
        rows={4}
      />
      <Button fullWidth type="submit" variant="contained" color="primary">
        Add Task
      </Button>
    </form>
  );
};

export default AddTask;
