export default interface Task {
  title: string;
  description: string;
  completed: boolean;
}
