# Task Management

##### task management with react

## Installation & Run Locally

Clone the project

```bash
  git clone https://gitlab.com/saberi.soheil74/task-management.git
```

Go to the project directory

```bash
  cd task-management
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```

Open http://localhost:3000 to view it in the browser.

## Tech Stack

**Client:** TypeScript, React, MUI

**Server:** Local
